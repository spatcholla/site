module.exports = {
  title: "Spatcholla's Blog",
  description: "This is a blog example built by VuePress",
  dest: "public",
  theme: "@vuepress/theme-blog",
  themeConfig: {
    /**
     * Ref: https://vuepress-theme-blog.ulivz.com/#modifyblogpluginoptions
     */
    modifyBlogPluginOptions(blogPluginOptions) {
      return blogPluginOptions
    },
    /**
     * Ref: https://vuepress-theme-blog.ulivz.com/#nav
     */
    nav: [
      {
        text: "Blog",
        link: "/",
      },
      {
        text: "Tags",
        link: "/tag/",
      },
    ],
    /**
     * Ref: https://vuepress-theme-blog.ulivz.com/#footer
     */
    footer: {
      contact: [
        {
          type: "gitlab",
          link: "https://gitlab.com/spatcholla",
        },
        {
          type: "twitter",
          link: "https://twitter.com/Spatcholla",
        },
      ],
      copyright: [
        {
          text: "Privacy Policy",
          link: "https://policies.google.com/privacy?hl=en-US",
        },
        {
          text: "MIT Licensed | Copyright © 2021-present Samuel Moore",
          link: "",
        },
      ],
    },
  },
}
